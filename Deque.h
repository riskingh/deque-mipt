//
//  Deque.h
//  Deque
//
//  Created by Максим Гришкин on 06/04/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __Deque__Deque__
#define __Deque__Deque__

#include <stdio.h>
#include <iostream>
#include <iterator>
#include <algorithm>

namespace NDeque {
    template <class _T>
    class CDeque {
    private:
        size_t capacity;
        _T *data, *left, *right;
        
        void fixCapacity() {
            capacity = std::max((size_t)3, (size_t)3 * ((size_t)(right - left)));
            _T *newData = new _T[(int)capacity];
            _T *newLeft = newData + std::max((size_t)(right - left), (size_t)1), *newRight;
            newRight = std::copy(left, right, newLeft);
            delete[] data;
            data = newData;
            left = newLeft;
            right = newRight;
        }
    public:
        CDeque() {
            data = left = right = NULL;
            capacity = 0;
        }
        
        ~CDeque() {
            delete[] data;
        }
        
        typedef _T* iterator;
        typedef _T* const const_iterator;
        typedef std::reverse_iterator<iterator> reverse_iterator;
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
        
        void push_front(_T _t) {
            if (data == left)
                fixCapacity();
            left--;
            *left = _t;
        }
        void push_back(_T _t) {
            if (data + capacity == right)
                fixCapacity();
            *right = _t;
            right++;
        }
        _T pop_front() {
            _T value = *left;
            left++;
            if ((size_t)(left - data) >= 2 * (size_t)(right - left))
                fixCapacity();
            return value;
        }
        _T pop_back() {
            right--;
            _T value = *right;
            if ((size_t)(data + capacity - right) >= 2 * (size_t)(right - left))
                fixCapacity();
            return value;
        }
        
        _T &front() {
            return *left;
        }
        const _T &front() const {
            return *left;
        }
        _T &back() {
            return *(right - 1);
        }
        const _T &back() const {
            return *(right - 1);
        }
        
        _T operator[](size_t _index) {
            return *(left + _index);
        }
        _T operator[](size_t _index) const {
            return *(left + _index);
        }
        
        
        bool empty() const {
            return left == right;
        }
        
        size_t size() const {
            return (size_t)(right - left);
        }
        
        
        iterator begin() {
            return left;
        }
        iterator begin() const {
            return left;
        }
        const_iterator cbegin() const {
            return left;
        }
        const_iterator end() {
            return right;
        }
        const_iterator end() const {
            return right;
        }
        const_iterator cend() const {
            return right;
        }
        
        reverse_iterator rbegin() {
            return reverse_iterator(right);
        }
        reverse_iterator rbegin() const {
            return reverse_iterator(right);
        }
        reverse_iterator rcbegin() const {
            return const_reverse_iterator(right);
        }
        reverse_iterator rend() {
            return reverse_iterator(left );
        }
        reverse_iterator rend() const {
            return reverse_iterator(left);
        }
        reverse_iterator rcend() {
            return const_reverse_iterator(left);
        }
        
        void show() {
            std::cout << left - data << " " << (data + capacity) - right << ": ";
            for (_T *iter = left; iter != right; ++iter) {
                std::cout << *iter << " ";
            }
            std::cout << "\n";
        }
        
    };
}

#endif /* defined(__Deque__Deque__) */
