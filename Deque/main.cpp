//
//  main.cpp
//  Deque
//
//  Created by Максим Гришкин on 06/04/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include <iostream>
#include <gtest/gtest.h>
#include <deque>
#include <ctime>
#include <random>
#include <algorithm>
#include "Deque.h"

enum EQueryType {
    QT_POP_FRONT,
    QT_POP_BACK,
    QT_FRONT,
    QT_BACK,
    QT_PUSH_FRONT,
    QT_PUSH_BACK,
    QT_EQUAL,
    QT_SIZE
};

class DequeTest: public testing::TestWithParam<size_t> {
};

class DequeTestTime: public testing::TestWithParam<size_t> {
};

TEST_P(DequeTest, TestEquality) {
    NDeque::CDeque<int> myDeque;
    std::deque<int> stdDeque;
    size_t currentSize = 0;
    std::default_random_engine generator((unsigned int)time(0));
    std::uniform_int_distribution<int> getQuery(0, (int)QT_SIZE - 1), getQueryForEmpty(4, (int)QT_SIZE - 1);
    std::uniform_int_distribution<int> randomInt(-1e9, 1e9);
    int forPush;
    for (size_t i = 0; i < GetParam(); ++i) {
        EQueryType queryType = (EQueryType)(currentSize ? getQuery(generator) : getQueryForEmpty(generator));
        switch (queryType) {
            case QT_POP_FRONT:
                myDeque.pop_front();
                stdDeque.pop_front();
                break;
            case QT_POP_BACK:
                myDeque.pop_back();
                stdDeque.pop_back();
                break;
            case QT_FRONT:
                EXPECT_EQ(myDeque.front(), stdDeque.front());
                break;
            case QT_BACK:
                EXPECT_EQ(myDeque.back(), stdDeque.back());
                break;
            case QT_PUSH_FRONT:
                forPush = randomInt(generator);
                myDeque.push_front(forPush);
                stdDeque.push_front(forPush);
                break;
            case QT_PUSH_BACK:
                forPush = randomInt(generator);
                myDeque.push_back(forPush);
                stdDeque.push_back(forPush);
                break;
            case QT_EQUAL:
                EXPECT_EQ(stdDeque.size(), myDeque.size());
                EXPECT_TRUE(std::equal(myDeque.begin(), myDeque.end(), stdDeque.begin()));
                EXPECT_TRUE(std::equal(myDeque.rbegin(), myDeque.rend(), stdDeque.rbegin()));
                break;
            default:
                ASSERT_TRUE(false);
                break;
        }
    }
    SUCCEED();
}

TEST_P(DequeTestTime, TestTime) {
    NDeque::CDeque<int> myDeque;
    size_t currentSize = 0;
    std::default_random_engine generator((unsigned int)time(0));
    std::uniform_int_distribution<int> getQuery(0, (int)QT_SIZE - 2), getQueryForEmpty(4, (int)QT_SIZE - 2);
    std::uniform_int_distribution<int> randomInt(-1e9, 1e9);
    int forPush;
    for (size_t i = 0; i < GetParam(); ++i) {
        EQueryType queryType = (EQueryType)(currentSize ? getQuery(generator) : getQueryForEmpty(generator));
        switch (queryType) {
            case QT_POP_FRONT:
                myDeque.pop_front();
                break;
            case QT_POP_BACK:
                myDeque.pop_back();
                break;
            case QT_PUSH_FRONT:
                forPush = randomInt(generator);
                myDeque.push_front(forPush);
                break;
            case QT_PUSH_BACK:
                forPush = randomInt(generator);
                myDeque.push_back(forPush);
                break;
            default:
                ASSERT_TRUE(false);
                break;
        }
    }
    SUCCEED();
}

INSTANTIATE_TEST_CASE_P(TestEqualityInst, DequeTest, testing::Values(1e0, 1e1, 1e2, 1e3, 1e4));
INSTANTIATE_TEST_CASE_P(TestTimeInst, DequeTestTime, testing::Values(1e0, 1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8));

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
